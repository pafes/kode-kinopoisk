package kode.kinopoisk.pavlov.view.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kode.kinopoisk.pavlov.R;
import kode.kinopoisk.pavlov.databinding.ItemFilmBinding;
import kode.kinopoisk.pavlov.presenter.Film;

public class FilmsAdapter extends BaseAdapter<Film, FilmsAdapter.FilmViewHolder>
{

    public FilmsAdapter(View.OnClickListener listener)
    {
        super(listener);
    }

    @Override
    public FilmViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ItemFilmBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_film, viewGroup, false);

        binding.getRoot().setOnClickListener(listener);

        return new FilmViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(FilmViewHolder viewHolder, int i)
    {
        Film user = list.get(i);

        viewHolder.setFilm(user);
    }

    public class FilmViewHolder extends RecyclerView.ViewHolder
    {

        private ItemFilmBinding binding;

        public FilmViewHolder(ItemFilmBinding binding)
        {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void setFilm(Film film)
        {
            binding.getRoot().setId(Integer.parseInt(film.getFilmID()));
            binding.setFilm(film);
        }

    }

}
