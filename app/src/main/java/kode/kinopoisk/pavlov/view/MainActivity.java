package kode.kinopoisk.pavlov.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import kode.kinopoisk.pavlov.R;
import kode.kinopoisk.pavlov.presenter.Film;
import kode.kinopoisk.pavlov.presenter.Genre;
import kode.kinopoisk.pavlov.presenter.GenresPresenter;
import kode.kinopoisk.pavlov.presenter.SortsPresenter;
import kode.kinopoisk.pavlov.presenter.TodayFilmsPresenter;
import kode.kinopoisk.pavlov.view.adapters.FilmsAdapter;
import kode.kinopoisk.pavlov.databinding.MainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ListView<Film>, PopupMenu.OnMenuItemClickListener
{

    private final static int REQUEST_CITY_ID_CODE = 1;

    private MainBinding binding;
    private SharedPreferences sharedPreferences;
    private FilmsAdapter adapter;
    private TodayFilmsPresenter todayFilmsPresenter;
    private SortsPresenter sortsPresenter;
    private GenresPresenter genresPresenter;
    private PopupMenu sortSubmenu;
    private AlertDialog genreFilterAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.todayFilms);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setHomeAsUpIndicator(R.mipmap.ic_action_map);
        actionBar.setDisplayHomeAsUpEnabled(true);

        binding = DataBindingUtil.setContentView(this, R.layout.main);
        adapter = new FilmsAdapter(this);
        sharedPreferences = getSharedPreferences("pref", Context.MODE_PRIVATE);
        sortsPresenter = new SortsPresenter(this);
        genresPresenter = new GenresPresenter(this);
        todayFilmsPresenter = new TodayFilmsPresenter(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setAdapter(adapter);

        if (sharedPreferences.contains("cityID"))
        {
            todayFilmsPresenter.setCityID(sharedPreferences.getInt("cityID", -1));
            todayFilmsPresenter.loadTodayFilms();
        }
        else
            startActivityForResult(new Intent(this, ChooseCityActivity.class), REQUEST_CITY_ID_CODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        switch (id)
        {
            case android.R.id.home:
                startActivityForResult(new Intent(this, ChooseCityActivity.class), REQUEST_CITY_ID_CODE);
                break;
            case R.id.action_sortBy:
                if (sortSubmenu == null || sortsPresenter.getSortList().size() == 0)
                    sortSubmenu = createSortSubmenu(findViewById(id));

                sortSubmenu.show();
                break;
            case R.id.action_genreFilter:
                if (genreFilterAlert == null || genresPresenter.getGenreList().size() == 0)
                    genreFilterAlert = createGenreFilterAlert();

                genreFilterAlert.show();
                break;
        }

        return true;
    }

    private PopupMenu createSortSubmenu(View view)
    {
        PopupMenu popup = new PopupMenu(this, view);
        Menu menu = popup.getMenu();

        for (String sort : sortsPresenter.getSortList())
            menu.add(sort);

        menu.setGroupCheckable(0, true, true);

        if (menu.size() > 0)
            menu.getItem(0).setChecked(true);

        popup.setOnMenuItemClickListener(this);

        return popup;
    }

    private AlertDialog createGenreFilterAlert()
    {
        List<Integer> genreIDs = new ArrayList<>();
        List<String> genreNames = new ArrayList<>();
        List<Genre> genreList = genresPresenter.getGenreList();

        for (Genre genre : genreList)
            genreNames.add(genre.getGenreName());

        return new AlertDialog.Builder(this)
            .setTitle(R.string.genre_filter)
            .setMultiChoiceItems(genreNames.toArray(new String[genreNames.size()]), null, (dialog, which, isChecked) -> {
                Integer genreID = Integer.valueOf(genreList.get(which).getGenreID());

                if (isChecked)
                    genreIDs.add(genreID);
                else
                    genreIDs.remove(genreID);
            })
            .setPositiveButton("Фильтровать", (dialog, which) -> {
                todayFilmsPresenter.setGenreIDs(genreIDs);
                todayFilmsPresenter.loadTodayFilms();
            })
            .setNegativeButton("Очистить", (dialog, which) -> {
                AlertDialog alertDialog = ((AlertDialog) dialog);
                android.widget.ListView list = alertDialog.getListView();

                for (int i = 0; i < list.getCount(); i++)
                    list.setItemChecked(i, false);

                genreIDs.clear();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).callOnClick();
            })
            .setNeutralButton(android.R.string.cancel, null)
            .create();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        item.setChecked(true);

        todayFilmsPresenter.setSortBy(item.getTitle().toString());
        todayFilmsPresenter.loadTodayFilms();

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode != REQUEST_CITY_ID_CODE)
            return;

        if (resultCode == Activity.RESULT_CANCELED)
            makeToast(getString(R.string.defaultCity));

        int cityID = data == null ? 1 : data.getIntExtra("cityID", 1);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("cityID", cityID);
        editor.apply();

        todayFilmsPresenter.setCityID(cityID);
        todayFilmsPresenter.loadTodayFilms();
    }

    @Override
    public void onClick(View v)
    {
        Intent detailFilmIntent = new Intent(this, DetailFilmActivity.class);

        detailFilmIntent.putExtra("filmID", v.getId());

        startActivity(detailFilmIntent);
    }

    @Override
    public void showList(List<Film> list)
    {
        adapter.setList(list);
    }

    @Override
    public void showEmptyList()
    {
        makeToast(getString(R.string.notFilmsForConditions));
    }

    private void makeToast(String text)
    {
        Snackbar.make(binding.recyclerView, text, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(String error)
    {
        makeToast(error);
    }

    @Override
    public void onStop()
    {
        super.onStop();

        if (todayFilmsPresenter != null)
            todayFilmsPresenter.onStop();

        if (sortsPresenter != null)
            sortsPresenter.onStop();

        if (genresPresenter != null)
            genresPresenter.onStop();
    }

}
