package kode.kinopoisk.pavlov.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.util.TypedValue;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import kode.kinopoisk.pavlov.R;
import kode.kinopoisk.pavlov.databinding.CinemaItemBinding;
import kode.kinopoisk.pavlov.presenter.Cinema;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class CinemaItem
{

    private static final int maxRowElements = 4;
    
    private LayoutInflater inflater;
    private CinemaItemBinding binding;

    public CinemaItem(Context context, Intent data)
    {
        inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(inflater, R.layout.cinema_item, null, false);

        Cinema cinema = (Cinema) data.getSerializableExtra("cinema");

        binding.setCinema(cinema);

        setCinemaData(binding.seances, cinema.getSeanceData(), maxRowElements);
    }

    private void setCinemaData(LinearLayout parent, List<String> seances, int maxRowElements)
    {
        int textViewPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, parent.getContext().getResources().getDisplayMetrics());

        for (int i = 0; i < seances.size(); i++)
        {
            TextView textView = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, null);

            textView.setText(seances.get(i));
            textView.setPadding(textViewPadding, 0, textViewPadding, 0);

            if (i % maxRowElements == 0)
                parent.addView(getHorizontalLinearLayout(parent.getContext()));

            ((LinearLayout)parent.getChildAt(parent.getChildCount() - 1)).addView(textView);
        }
    }

    private LinearLayout getHorizontalLinearLayout(Context context)
    {
        LinearLayout innerLayout = new LinearLayout(context);
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        innerLayout.setLayoutParams(linearLayoutParams);

        return innerLayout;
    }

    public void setOnClickListener(android.view.View.OnClickListener listener)
    {
        binding.cinemaName.setOnClickListener(listener);
    }

    public android.view.View getView()
    {
        return binding.getRoot();
    }

}
