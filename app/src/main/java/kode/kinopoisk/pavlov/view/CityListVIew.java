package kode.kinopoisk.pavlov.view;

import kode.kinopoisk.pavlov.presenter.City;

public interface CityListView extends ListView<City>
{

    String getCityName();

}
