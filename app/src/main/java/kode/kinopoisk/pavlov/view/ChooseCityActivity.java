package kode.kinopoisk.pavlov.view;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import kode.kinopoisk.pavlov.R;
import kode.kinopoisk.pavlov.presenter.City;
import kode.kinopoisk.pavlov.presenter.CitiesPresenter;
import kode.kinopoisk.pavlov.view.adapters.CitiesAdapter;
import kode.kinopoisk.pavlov.databinding.ChooseCityBinding;

public class ChooseCityActivity extends AppCompatActivity implements View.OnClickListener, CityListView
{

    private ChooseCityBinding binding;
    private CitiesPresenter citiesPresenter;
    private CitiesAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        binding = DataBindingUtil.setContentView(this, R.layout.choose_city);
        adapter = new CitiesAdapter(this);
        citiesPresenter = new CitiesPresenter(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setAdapter(adapter);
        binding.editText.setOnEditorActionListener(citiesPresenter);
        binding.editText.addTextChangedListener(citiesPresenter.textChangedListener);

        citiesPresenter.initCityList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
            finish();

        return true;
    }

    public void onClearClicked(View view)
    {
        binding.editText.getText().clear();
    }

    @Override
    public void onClick(View v)
    {
        Intent resultIntent = new Intent();

        resultIntent.putExtra("cityID", v.getId());

        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void showList(List<City> cityList)
    {
        adapter.setList(cityList);
    }

    @Override
    public void showEmptyList()
    {
        makeToast(getString(R.string.empty_list));
    }

    private void makeToast(String text)
    {
        Snackbar.make(binding.recyclerView, text, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public String getCityName()
    {
        return binding.editText.getText().toString();
    }

    @Override
    public void showError(String error)
    {
        makeToast(error);
    }

    @Override
    public void onStop()
    {
        super.onStop();

        if (citiesPresenter != null)
            citiesPresenter.onStop();
    }

}
