package kode.kinopoisk.pavlov.view;

import kode.kinopoisk.pavlov.presenter.DetailFilm;

public interface DetailFilmView extends View
{

    void showDetailFilm(DetailFilm detailFilm);

}
