package kode.kinopoisk.pavlov.view;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import kode.kinopoisk.pavlov.R;
import kode.kinopoisk.pavlov.databinding.FullScreenImageBinding;

public class FullScreenImageActivity extends AppCompatActivity
{

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        byte[] bytes = getIntent().getByteArrayExtra("imageBytes");
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        FullScreenImageBinding binding = DataBindingUtil.setContentView(this, R.layout.full_screen_image);

        binding.imageView.setImageBitmap(bitmap);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
            finish();

        return true;
    }

}
