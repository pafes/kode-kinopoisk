package kode.kinopoisk.pavlov.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;

import kode.kinopoisk.pavlov.util.DownloadImageTask;

public class ImageViewFragment extends Fragment implements android.view.View.OnClickListener
{

    private Context context;
    private ImageView imageView;

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        String url = getArguments().getString("url");

        context = parent.getContext();
        imageView = new ImageView(parent.getContext());

        imageView.setOnClickListener(this);

        new DownloadImageTask(imageView).execute(url);

        return imageView;
    }

    @Override
    public void onClick(android.view.View v)
    {
        Intent fullScreenImageIntent = new Intent(context, FullScreenImageActivity.class);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
        fullScreenImageIntent.putExtra("imageBytes", byteArrayOutputStream.toByteArray());

        startActivity(fullScreenImageIntent);
    }

}
