package kode.kinopoisk.pavlov.view.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import kode.kinopoisk.pavlov.view.ImageViewFragment;

public class GalleryPagerAdapter extends FragmentStatePagerAdapter
{

    private List<String> urls;

    public GalleryPagerAdapter(FragmentManager fm, List<String> urls)
    {
        super(fm);

        this.urls = urls;
    }

    @Override
    public Fragment getItem(int position)
    {
        Fragment imageFragment = new ImageViewFragment();
        Bundle arguments = new Bundle();

        arguments.putString("url", urls.get(position));
        imageFragment.setArguments(arguments);

        return imageFragment;
    }

    @Override
    public int getCount()
    {
        return urls.size();
    }

}
