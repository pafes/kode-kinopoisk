package kode.kinopoisk.pavlov.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import kode.kinopoisk.pavlov.R;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback
{

    private double lng;
    private double lat;
    private String cinemaName;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_layout);

        Intent locationData = getIntent();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        lng = Double.parseDouble(locationData.getStringExtra("lng"));
        lat = Double.parseDouble(locationData.getStringExtra("lat"));
        cinemaName = locationData.getStringExtra("cinemaName");

        mapFragment.getMapAsync(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(cinemaName);
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        LatLng cinema = new LatLng(lat, lng);

        googleMap.addMarker(new MarkerOptions().position(cinema).title(cinemaName));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cinema, 12.0f));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
            finish();

        return true;
    }

}
