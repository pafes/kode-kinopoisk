package kode.kinopoisk.pavlov.view;

public interface View
{

    void showError(String error);

}
