package kode.kinopoisk.pavlov.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TableLayout;

import java.util.List;

import kode.kinopoisk.pavlov.R;
import kode.kinopoisk.pavlov.databinding.DetailFilmBinding;
import kode.kinopoisk.pavlov.presenter.Cinema;
import kode.kinopoisk.pavlov.presenter.DetailFilm;
import kode.kinopoisk.pavlov.presenter.DetailFilmPresenter;
import kode.kinopoisk.pavlov.util.DownloadImageTask;
import kode.kinopoisk.pavlov.view.adapters.GalleryPagerAdapter;

public class DetailFilmActivity extends AppCompatActivity implements DetailFilmView
{

    private DetailFilmBinding binding;
    private DetailFilmPresenter detailFilmPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.detail_film);
        detailFilmPresenter = new DetailFilmPresenter(this);

        Bundle bundle = getIntent().getExtras();
        int filmID = bundle.getInt("filmID");

        detailFilmPresenter.loadFilm(filmID);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showDetailFilm(DetailFilm detailFilm)
    {
        setTitle(detailFilm.getName());
        binding.setFilm(detailFilm);

        new DownloadImageTask(binding.poster).execute(detailFilm.getPosterURL());

        GalleryPagerAdapter pagerAdapter = new GalleryPagerAdapter(getSupportFragmentManager(), detailFilm.getGallery());

        binding.gallery.setAdapter(pagerAdapter);

        createSeances(binding.seances, detailFilm.getCinemaData());
    }

    private void createSeances(TableLayout seances, List<Cinema> cinemaData)
    {
        for (Cinema cinema : cinemaData)
        {
            Intent cinemaIntent = new Intent();

            cinemaIntent.putExtra("cinema", cinema);

            CinemaItem cinemaItem = new CinemaItem(this, cinemaIntent);

            cinemaItem.setOnClickListener(v -> {
                Intent cinemaPositionIntent = new Intent(this, MapActivity.class);

                cinemaPositionIntent.putExtra("lng", cinema.getCinemaLocation().getLon());
                cinemaPositionIntent.putExtra("lat", cinema.getCinemaLocation().getLat());
                cinemaPositionIntent.putExtra("cinemaName", cinema.getCinemaName());

                startActivity(cinemaPositionIntent);
            });

            seances.addView(cinemaItem.getView());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
            finish();

        return true;
    }

    @Override
    public void showError(String error)
    {
        makeToast(error);
    }

    private void makeToast(String text)
    {
        Snackbar.make(binding.getRoot(), text, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onStop()
    {
        super.onStop();

        if (detailFilmPresenter != null)
            detailFilmPresenter.onStop();
    }

}
