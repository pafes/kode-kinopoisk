
package kode.kinopoisk.pavlov.model.dto;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CinemaDTO {

    @SerializedName("cinemaName")
    @Expose
    private String cinemaName;
    @SerializedName("seanceData")
    @Expose
    private List<String> seanceData = new ArrayList<String>();
    @SerializedName("cinemaLocation")
    @Expose
    private CinemaLocationDTO cinemaLocationDTO;

    /**
     * 
     * @return
     *     The cinemaName
     */
    public String getCinemaName() {
        return cinemaName;
    }

    /**
     * 
     * @param cinemaName
     *     The cinemaName
     */
    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    /**
     * 
     * @return
     *     The seanceData
     */
    public List<String> getSeanceData() {
        return seanceData;
    }

    /**
     * 
     * @param seanceData
     *     The seanceData
     */
    public void setSeanceData(List<String> seanceData) {
        this.seanceData = seanceData;
    }

    /**
     * 
     * @return
     *     The cinemaLocation
     */
    public CinemaLocationDTO getCinemaLocationDTO() {
        return cinemaLocationDTO;
    }

    /**
     * 
     * @param cinemaLocationDTO
     *     The cinemaLocation
     */
    public void setCinemaLocationDTO(CinemaLocationDTO cinemaLocationDTO) {
        this.cinemaLocationDTO = cinemaLocationDTO;
    }

}
