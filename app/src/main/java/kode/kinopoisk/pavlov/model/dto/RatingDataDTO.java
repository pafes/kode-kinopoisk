
package kode.kinopoisk.pavlov.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatingDataDTO {

    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("ratingVoteCount")
    @Expose
    private String ratingVoteCount;
    @SerializedName("ratingAwait")
    @Expose
    private String ratingAwait;
    @SerializedName("ratingAwaitCount")
    @Expose
    private String ratingAwaitCount;
    @SerializedName("ratingRFCritics")
    @Expose
    private String ratingRFCritics;
    @SerializedName("ratingRFCriticsVoteCount")
    @Expose
    private String ratingRFCriticsVoteCount;

    /**
     * 
     * @return
     *     The rating
     */
    public String getRating() {
        return rating;
    }

    /**
     * 
     * @param rating
     *     The rating
     */
    public void setRating(String rating) {
        this.rating = rating;
    }

    /**
     * 
     * @return
     *     The ratingVoteCount
     */
    public String getRatingVoteCount() {
        return ratingVoteCount;
    }

    /**
     * 
     * @param ratingVoteCount
     *     The ratingVoteCount
     */
    public void setRatingVoteCount(String ratingVoteCount) {
        this.ratingVoteCount = ratingVoteCount;
    }

    /**
     * 
     * @return
     *     The ratingAwait
     */
    public String getRatingAwait() {
        return ratingAwait;
    }

    /**
     * 
     * @param ratingAwait
     *     The ratingAwait
     */
    public void setRatingAwait(String ratingAwait) {
        this.ratingAwait = ratingAwait;
    }

    /**
     * 
     * @return
     *     The ratingAwaitCount
     */
    public String getRatingAwaitCount() {
        return ratingAwaitCount;
    }

    /**
     * 
     * @param ratingAwaitCount
     *     The ratingAwaitCount
     */
    public void setRatingAwaitCount(String ratingAwaitCount) {
        this.ratingAwaitCount = ratingAwaitCount;
    }

    /**
     * 
     * @return
     *     The ratingRFCritics
     */
    public String getRatingRFCritics() {
        return ratingRFCritics;
    }

    /**
     * 
     * @param ratingRFCritics
     *     The ratingRFCritics
     */
    public void setRatingRFCritics(String ratingRFCritics) {
        this.ratingRFCritics = ratingRFCritics;
    }

    /**
     * 
     * @return
     *     The ratingRFCriticsVoteCount
     */
    public String getRatingRFCriticsVoteCount() {
        return ratingRFCriticsVoteCount;
    }

    /**
     * 
     * @param ratingRFCriticsVoteCount
     *     The ratingRFCriticsVoteCount
     */
    public void setRatingRFCriticsVoteCount(String ratingRFCriticsVoteCount) {
        this.ratingRFCriticsVoteCount = ratingRFCriticsVoteCount;
    }

}
