package kode.kinopoisk.pavlov.model;

import android.text.TextUtils;

import kode.kinopoisk.pavlov.model.api.ApiInterface;
import kode.kinopoisk.pavlov.model.dto.*;

import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ModelImpl implements Model
{

    private final Observable.Transformer schedulersTransformer;

    private ApiInterface apiInterface;
    private Scheduler uiThread;
    private Scheduler ioThread;

    public ModelImpl(ApiInterface apiInterface)
    {
        this.apiInterface = apiInterface;

        uiThread = AndroidSchedulers.mainThread();
        ioThread = Schedulers.io();
        schedulersTransformer = o -> ((Observable) o).subscribeOn(ioThread)
            .observeOn(uiThread)
            .unsubscribeOn(ioThread);
    }

    @Override
    public Observable<CityListDTO> getCityList()
    {
        return apiInterface
            .getCityList(2)
            .compose(applySchedulers());
    }

    @Override
    public Observable<GenresDTO> getGenres()
    {
        return apiInterface
            .getGenres()
            .compose(applySchedulers());
    }

    @Override
    public Observable<SortsDTO> getSorts()
    {
        return apiInterface
            .getSorts()
            .compose(applySchedulers());
    }

    @Override
    public Observable<TodayFilmsDTO> getTodayFilms(int cityID, List<Integer> genreIDs, String sortBy)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        Date date = new Date();
        String today = dateFormat.format(date);

        return apiInterface
            .getTodayFilms(cityID, TextUtils.join(",", genreIDs), sortBy, today)
            .compose(applySchedulers());
    }

    @Override
    public Observable<DetailFilmDTO> getDetailFilm(int filmID)
    {
        return apiInterface
            .getDetailFilm(filmID)
            .compose(applySchedulers());
    }

    @SuppressWarnings("unchecked")
    private <T> Observable.Transformer<T, T> applySchedulers()
    {
        return (Observable.Transformer<T, T>) schedulersTransformer;
    }

}
