
package kode.kinopoisk.pavlov.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenreDTO {

    @SerializedName("genreID")
    @Expose
    private String genreID;
    @SerializedName("genreName")
    @Expose
    private String genreName;

    /**
     * 
     * @return
     *     The genreID
     */
    public String getGenreID() {
        return genreID;
    }

    /**
     * 
     * @param genreID
     *     The genreID
     */
    public void setGenreID(String genreID) {
        this.genreID = genreID;
    }

    /**
     * 
     * @return
     *     The genreName
     */
    public String getGenreName() {
        return genreName;
    }

    /**
     * 
     * @param genreName
     *     The genreName
     */
    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

}
