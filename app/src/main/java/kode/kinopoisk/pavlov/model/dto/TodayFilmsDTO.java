
package kode.kinopoisk.pavlov.model.dto;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TodayFilmsDTO {

    @SerializedName("filmData")
    @Expose
    private List<FilmDTO> filmData = new ArrayList<FilmDTO>();

    /**
     * 
     * @return
     *     The filmData
     */
    public List<FilmDTO> getFilmData() {
        return filmData;
    }

    /**
     * 
     * @param filmData
     *     The filmData
     */
    public void setFilmData(List<FilmDTO> filmData) {
        this.filmData = filmData;
    }

}
