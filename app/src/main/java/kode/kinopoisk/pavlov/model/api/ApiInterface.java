package kode.kinopoisk.pavlov.model.api;

import kode.kinopoisk.pavlov.model.dto.*;

import retrofit2.http.GET;
import retrofit2.http.Query;

import rx.Observable;

public interface ApiInterface {

    @GET("getCityList")
    Observable<CityListDTO> getCityList(@Query("countryID") int countryID);

    @GET("getGenres")
    Observable<GenresDTO> getGenres();

    @GET("getSorts")
    Observable<SortsDTO> getSorts();

    @GET("getTodayFilms")
    Observable<TodayFilmsDTO> getTodayFilms(@Query("cityID") int cityID, @Query("genreIDs") String genreIDs, @Query("sortBy") String sortBy, @Query("date") String date);

    @GET("getDetailFilm")
    Observable<DetailFilmDTO> getDetailFilm(@Query("filmID") int filmID);

}
