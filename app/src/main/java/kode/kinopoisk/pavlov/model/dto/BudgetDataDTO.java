
package kode.kinopoisk.pavlov.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BudgetDataDTO {

    @SerializedName("grossRU")
    @Expose
    private String grossRU;
    @SerializedName("budget")
    @Expose
    private String budget;

    /**
     * 
     * @return
     *     The grossRU
     */
    public String getGrossRU() {
        return grossRU;
    }

    /**
     * 
     * @param grossRU
     *     The grossRU
     */
    public void setGrossRU(String grossRU) {
        this.grossRU = grossRU;
    }

    /**
     * 
     * @return
     *     The budget
     */
    public String getBudget() {
        return budget;
    }

    /**
     * 
     * @param budget
     *     The budget
     */
    public void setBudget(String budget) {
        this.budget = budget;
    }

}
