
package kode.kinopoisk.pavlov.model.dto;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreatorsDTO {

    @SerializedName("directors")
    @Expose
    private List<String> directors = new ArrayList<String>();
    @SerializedName("actors")
    @Expose
    private List<String> actors = new ArrayList<String>();
    @SerializedName("producers")
    @Expose
    private List<String> producers = new ArrayList<String>();

    /**
     * 
     * @return
     *     The directors
     */
    public List<String> getDirectors() {
        return directors;
    }

    /**
     * 
     * @param directors
     *     The directors
     */
    public void setDirectors(List<String> directors) {
        this.directors = directors;
    }

    /**
     * 
     * @return
     *     The actors
     */
    public List<String> getActors() {
        return actors;
    }

    /**
     * 
     * @param actors
     *     The actors
     */
    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    /**
     * 
     * @return
     *     The producers
     */
    public List<String> getProducers() {
        return producers;
    }

    /**
     * 
     * @param producers
     *     The producers
     */
    public void setProducers(List<String> producers) {
        this.producers = producers;
    }

}
