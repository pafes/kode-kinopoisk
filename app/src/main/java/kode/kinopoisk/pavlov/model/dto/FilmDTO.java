
package kode.kinopoisk.pavlov.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilmDTO {

    @SerializedName("filmID")
    @Expose
    private String filmID;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("webURL")
    @Expose
    private String webURL;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("genre")
    @Expose
    private String genre;
    @SerializedName("filmLength")
    @Expose
    private String filmLength;

    /**
     * 
     * @return
     *     The filmID
     */
    public String getFilmID() {
        return filmID;
    }

    /**
     * 
     * @param filmID
     *     The filmID
     */
    public void setFilmID(String filmID) {
        this.filmID = filmID;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The webURL
     */
    public String getWebURL() {
        return webURL;
    }

    /**
     * 
     * @param webURL
     *     The webURL
     */
    public void setWebURL(String webURL) {
        this.webURL = webURL;
    }

    /**
     * 
     * @return
     *     The rating
     */
    public String getRating() {
        return rating;
    }

    /**
     * 
     * @param rating
     *     The rating
     */
    public void setRating(String rating) {
        this.rating = rating;
    }

    /**
     * 
     * @return
     *     The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The year
     */
    public String getYear() {
        return year;
    }

    /**
     * 
     * @param year
     *     The year
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * 
     * @return
     *     The genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * 
     * @param genre
     *     The genre
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * 
     * @return
     *     The filmLength
     */
    public String getFilmLength() {
        return filmLength;
    }

    /**
     * 
     * @param filmLength
     *     The filmLength
     */
    public void setFilmLength(String filmLength) {
        this.filmLength = filmLength;
    }

}
