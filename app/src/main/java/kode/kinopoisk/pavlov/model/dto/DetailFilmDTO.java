
package kode.kinopoisk.pavlov.model.dto;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailFilmDTO {

    @SerializedName("ratingData")
    @Expose
    private RatingDataDTO ratingDataDTO;
    @SerializedName("filmID")
    @Expose
    private String filmID;
    @SerializedName("posterURL")
    @Expose
    private String posterURL;
    @SerializedName("webURL")
    @Expose
    private String webURL;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("filmLength")
    @Expose
    private String filmLength;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("genre")
    @Expose
    private String genre;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("ratingMPAA")
    @Expose
    private String ratingMPAA;
    @SerializedName("ratingAgeLimits")
    @Expose
    private String ratingAgeLimits;
    @SerializedName("rentData")
    @Expose
    private RentDataDTO rentDataDTO;
    @SerializedName("budgetData")
    @Expose
    private BudgetDataDTO budgetDataDTO;
    @SerializedName("gallery")
    @Expose
    private List<String> gallery = new ArrayList<String>();
    @SerializedName("creators")
    @Expose
    private CreatorsDTO creatorsDTO;
    @SerializedName("cinemaData")
    @Expose
    private List<CinemaDTO> cinemaData = new ArrayList<CinemaDTO>();

    /**
     * 
     * @return
     *     The ratingData
     */
    public RatingDataDTO getRatingDataDTO() {
        return ratingDataDTO;
    }

    /**
     * 
     * @param ratingDataDTO
     *     The ratingData
     */
    public void setRatingDataDTO(RatingDataDTO ratingDataDTO) {
        this.ratingDataDTO = ratingDataDTO;
    }

    /**
     * 
     * @return
     *     The filmID
     */
    public String getFilmID() {
        return filmID;
    }

    /**
     * 
     * @param filmID
     *     The filmID
     */
    public void setFilmID(String filmID) {
        this.filmID = filmID;
    }

    /**
     * 
     * @return
     *     The posterURL
     */
    public String getPosterURL() {
        return posterURL;
    }

    /**
     * 
     * @param posterURL
     *     The posterURL
     */
    public void setPosterURL(String posterURL) {
        this.posterURL = posterURL;
    }

    /**
     * 
     * @return
     *     The webURL
     */
    public String getWebURL() {
        return webURL;
    }

    /**
     * 
     * @param webURL
     *     The webURL
     */
    public void setWebURL(String webURL) {
        this.webURL = webURL;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The year
     */
    public String getYear() {
        return year;
    }

    /**
     * 
     * @param year
     *     The year
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * 
     * @return
     *     The filmLength
     */
    public String getFilmLength() {
        return filmLength;
    }

    /**
     * 
     * @param filmLength
     *     The filmLength
     */
    public void setFilmLength(String filmLength) {
        this.filmLength = filmLength;
    }

    /**
     * 
     * @return
     *     The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * 
     * @param genre
     *     The genre
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The ratingMPAA
     */
    public String getRatingMPAA() {
        return ratingMPAA;
    }

    /**
     * 
     * @param ratingMPAA
     *     The ratingMPAA
     */
    public void setRatingMPAA(String ratingMPAA) {
        this.ratingMPAA = ratingMPAA;
    }

    /**
     * 
     * @return
     *     The ratingAgeLimits
     */
    public String getRatingAgeLimits() {
        return ratingAgeLimits;
    }

    /**
     * 
     * @param ratingAgeLimits
     *     The ratingAgeLimits
     */
    public void setRatingAgeLimits(String ratingAgeLimits) {
        this.ratingAgeLimits = ratingAgeLimits;
    }

    /**
     * 
     * @return
     *     The rentData
     */
    public RentDataDTO getRentDataDTO() {
        return rentDataDTO;
    }

    /**
     * 
     * @param rentDataDTO
     *     The rentData
     */
    public void setRentDataDTO(RentDataDTO rentDataDTO) {
        this.rentDataDTO = rentDataDTO;
    }

    /**
     * 
     * @return
     *     The budgetData
     */
    public BudgetDataDTO getBudgetDataDTO() {
        return budgetDataDTO;
    }

    /**
     * 
     * @param budgetDataDTO
     *     The budgetData
     */
    public void setBudgetDataDTO(BudgetDataDTO budgetDataDTO) {
        this.budgetDataDTO = budgetDataDTO;
    }

    /**
     * 
     * @return
     *     The gallery
     */
    public List<String> getGallery() {
        return gallery;
    }

    /**
     * 
     * @param gallery
     *     The gallery
     */
    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    /**
     * 
     * @return
     *     The creators
     */
    public CreatorsDTO getCreatorsDTO() {
        return creatorsDTO;
    }

    /**
     * 
     * @param creatorsDTO
     *     The creators
     */
    public void setCreatorsDTO(CreatorsDTO creatorsDTO) {
        this.creatorsDTO = creatorsDTO;
    }

    /**
     * 
     * @return
     *     The cinemaData
     */
    public List<CinemaDTO> getCinemaData() {
        return cinemaData;
    }

    /**
     * 
     * @param cinemaData
     *     The cinemaData
     */
    public void setCinemaData(List<CinemaDTO> cinemaData) {
        this.cinemaData = cinemaData;
    }

}
