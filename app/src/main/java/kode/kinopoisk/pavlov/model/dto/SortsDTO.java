
package kode.kinopoisk.pavlov.model.dto;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SortsDTO {

    @SerializedName("sortData")
    @Expose
    private List<String> sortData = new ArrayList<>();

    /**
     * 
     * @return
     *     The sortData
     */
    public List<String> getSortData() {
        return sortData;
    }

    /**
     * 
     * @param sortData
     *     The sortData
     */
    public void setSortData(List<String> sortData) {
        this.sortData = sortData;
    }

}
