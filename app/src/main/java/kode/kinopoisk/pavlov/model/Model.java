package kode.kinopoisk.pavlov.model;

import java.util.ArrayList;
import java.util.List;

import kode.kinopoisk.pavlov.model.dto.*;
import rx.Observable;

public interface Model {

    Observable<CityListDTO> getCityList();
    Observable<GenresDTO> getGenres();
    Observable<SortsDTO> getSorts();
    Observable<TodayFilmsDTO> getTodayFilms(int cityID, List<Integer> genreIDs, String sortBy);
    Observable<DetailFilmDTO> getDetailFilm(int filmID);

}
