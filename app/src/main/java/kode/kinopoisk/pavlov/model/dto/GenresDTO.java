
package kode.kinopoisk.pavlov.model.dto;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenresDTO {

    @SerializedName("genreData")
    @Expose
    private List<GenreDTO> genreData = new ArrayList<>();

    /**
     * 
     * @return
     *     The genreData
     */
    public List<GenreDTO> getGenreData() {
        return genreData;
    }

    /**
     * 
     * @param genreData
     *     The genreData
     */
    public void setGenreData(List<GenreDTO> genreData) {
        this.genreData = genreData;
    }

}
