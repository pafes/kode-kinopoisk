package kode.kinopoisk.pavlov.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

import kode.kinopoisk.pavlov.R;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap>
{

    private ImageView bmImage;

    public DownloadImageTask(ImageView bmImage)
    {
        this.bmImage = bmImage;

        bmImage.setImageResource(R.mipmap.ic_film_stub);
    }

    protected Bitmap doInBackground(String... urls)
    {
        String url = urls[0];
        Bitmap image = null;

        try
        {
            InputStream in = new java.net.URL(url).openStream();
            image = BitmapFactory.decodeStream(in);
        }
        catch (Exception e)
        {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        return image;
    }

    protected void onPostExecute(Bitmap result)
    {
        bmImage.setImageBitmap(result);
    }

}
