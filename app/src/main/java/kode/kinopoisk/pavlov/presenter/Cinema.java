package kode.kinopoisk.pavlov.presenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Cinema implements Serializable {

    private String cinemaName;
    private List<String> seanceData = new ArrayList<>();
    private CinemaLocation cinemaLocation;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Cinema() {
    }

    /**
     * 
     * @param seanceData
     * @param cinemaName
     * @param cinemaLocation
     */
    public Cinema(String cinemaName, List<String> seanceData, CinemaLocation cinemaLocation) {
        this.cinemaName = cinemaName;
        this.seanceData = seanceData;
        this.cinemaLocation = cinemaLocation;
    }

    /**
     * 
     * @return
     *     The cinemaName
     */
    public String getCinemaName() {
        return cinemaName;
    }

    /**
     * 
     * @param cinemaName
     *     The cinemaName
     */
    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    /**
     * 
     * @return
     *     The seanceData
     */
    public List<String> getSeanceData() {
        return seanceData;
    }

    /**
     * 
     * @param seanceData
     *     The seanceData
     */
    public void setSeanceData(List<String> seanceData) {
        this.seanceData = seanceData;
    }

    /**
     * 
     * @return
     *     The cinemaLocation
     */
    public CinemaLocation getCinemaLocation() {
        return cinemaLocation;
    }

    /**
     * 
     * @param cinemaLocation
     *     The cinemaLocation
     */
    public void setCinemaLocation(CinemaLocation cinemaLocation) {
        this.cinemaLocation = cinemaLocation;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(cinemaName).append(seanceData).append(cinemaLocation).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;

        if (!(other instanceof Cinema))
            return false;

        Cinema rhs = ((Cinema) other);

        return new EqualsBuilder().append(cinemaName, rhs.cinemaName).append(seanceData, rhs.seanceData).append(cinemaLocation, rhs.cinemaLocation).isEquals();
    }

}
