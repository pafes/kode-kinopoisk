
package kode.kinopoisk.pavlov.presenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DetailFilm implements Serializable {

    private RatingData ratingData;
    private String filmID;
    private String posterURL;
    private String webURL;
    private String name;
    private String year;
    private String filmLength;
    private String country;
    private String genre;
    private String description;
    private String ratingMPAA;
    private String ratingAgeLimits;
    private RentData rentData;
    private BudgetData budgetData;
    private List<String> gallery = new ArrayList<>();
    private Creators creators;
    private List<Cinema> cinemaData = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public DetailFilm() {
    }

    /**
     * 
     * @param genre
     * @param budgetData
     * @param cinemaData
     * @param gallery
     * @param ratingMPAA
     * @param ratingAgeLimits
     * @param country
     * @param creators
     * @param filmLength
     * @param posterURL
     * @param description
     * @param name
     * @param webURL
     * @param rentData
     * @param year
     * @param ratingData
     * @param filmID
     */
    public DetailFilm(RatingData ratingData, String filmID, String posterURL, String webURL, String name, String year, String filmLength, String country, String genre, String description, String ratingMPAA, String ratingAgeLimits, RentData rentData, BudgetData budgetData, List<String> gallery, Creators creators, List<Cinema> cinemaData) {
        this.ratingData = ratingData;
        this.filmID = filmID;
        this.posterURL = posterURL;
        this.webURL = webURL;
        this.name = name;
        this.year = year;
        this.filmLength = filmLength;
        this.country = country;
        this.genre = genre;
        this.description = description;
        this.ratingMPAA = ratingMPAA;
        this.ratingAgeLimits = ratingAgeLimits;
        this.rentData = rentData;
        this.budgetData = budgetData;
        this.gallery = gallery;
        this.creators = creators;
        this.cinemaData = cinemaData;
    }

    /**
     * 
     * @return
     *     The ratingData
     */
    public RatingData getRatingData() {
        return ratingData;
    }

    /**
     * 
     * @param ratingData
     *     The ratingData
     */
    public void setRatingData(RatingData ratingData) {
        this.ratingData = ratingData;
    }

    /**
     * 
     * @return
     *     The filmID
     */
    public String getFilmID() {
        return filmID;
    }

    /**
     * 
     * @param filmID
     *     The filmID
     */
    public void setFilmID(String filmID) {
        this.filmID = filmID;
    }

    /**
     * 
     * @return
     *     The posterURL
     */
    public String getPosterURL() {
        return posterURL;
    }

    /**
     * 
     * @param posterURL
     *     The posterURL
     */
    public void setPosterURL(String posterURL) {
        this.posterURL = posterURL;
    }

    /**
     * 
     * @return
     *     The webURL
     */
    public String getWebURL() {
        return webURL;
    }

    /**
     * 
     * @param webURL
     *     The webURL
     */
    public void setWebURL(String webURL) {
        this.webURL = webURL;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The year
     */
    public String getYear() {
        return year;
    }

    /**
     * 
     * @param year
     *     The year
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * 
     * @return
     *     The filmLength
     */
    public String getFilmLength() {
        return filmLength;
    }

    /**
     * 
     * @param filmLength
     *     The filmLength
     */
    public void setFilmLength(String filmLength) {
        this.filmLength = filmLength;
    }

    /**
     * 
     * @return
     *     The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * 
     * @param genre
     *     The genre
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The ratingMPAA
     */
    public String getRatingMPAA() {
        return ratingMPAA;
    }

    /**
     * 
     * @param ratingMPAA
     *     The ratingMPAA
     */
    public void setRatingMPAA(String ratingMPAA) {
        this.ratingMPAA = ratingMPAA;
    }

    /**
     * 
     * @return
     *     The ratingAgeLimits
     */
    public String getRatingAgeLimits() {
        return ratingAgeLimits;
    }

    /**
     * 
     * @param ratingAgeLimits
     *     The ratingAgeLimits
     */
    public void setRatingAgeLimits(String ratingAgeLimits) {
        this.ratingAgeLimits = ratingAgeLimits;
    }

    /**
     * 
     * @return
     *     The rentData
     */
    public RentData getRentData() {
        return rentData;
    }

    /**
     * 
     * @param rentData
     *     The rentData
     */
    public void setRentData(RentData rentData) {
        this.rentData = rentData;
    }

    /**
     * 
     * @return
     *     The budgetData
     */
    public BudgetData getBudgetData() {
        return budgetData;
    }

    /**
     * 
     * @param budgetData
     *     The budgetData
     */
    public void setBudgetData(BudgetData budgetData) {
        this.budgetData = budgetData;
    }

    /**
     * 
     * @return
     *     The gallery
     */
    public List<String> getGallery() {
        return gallery;
    }

    /**
     * 
     * @param gallery
     *     The gallery
     */
    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    /**
     * 
     * @return
     *     The creators
     */
    public Creators getCreators() {
        return creators;
    }

    /**
     * 
     * @param creators
     *     The creators
     */
    public void setCreators(Creators creators) {
        this.creators = creators;
    }

    /**
     * 
     * @return
     *     The cinemaData
     */
    public List<Cinema> getCinemaData() {
        return cinemaData;
    }

    /**
     * 
     * @param cinemaData
     *     The cinemaData
     */
    public void setCinemaData(List<Cinema> cinemaData) {
        this.cinemaData = cinemaData;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(ratingData).append(filmID).append(posterURL).append(webURL).append(name).append(year).append(filmLength).append(country).append(genre).append(description).append(ratingMPAA).append(ratingAgeLimits).append(rentData).append(budgetData).append(gallery).append(creators).append(cinemaData).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;

        if (!(other instanceof DetailFilm))
            return false;

        DetailFilm rhs = ((DetailFilm) other);

        return new EqualsBuilder().append(ratingData, rhs.ratingData).append(filmID, rhs.filmID).append(posterURL, rhs.posterURL).append(webURL, rhs.webURL).append(name, rhs.name).append(year, rhs.year).append(filmLength, rhs.filmLength).append(country, rhs.country).append(genre, rhs.genre).append(description, rhs.description).append(ratingMPAA, rhs.ratingMPAA).append(ratingAgeLimits, rhs.ratingAgeLimits).append(rentData, rhs.rentData).append(budgetData, rhs.budgetData).append(gallery, rhs.gallery).append(creators, rhs.creators).append(cinemaData, rhs.cinemaData).isEquals();
    }

}
