package kode.kinopoisk.pavlov.presenter.mappers;

import java.util.List;

import kode.kinopoisk.pavlov.model.dto.SortsDTO;
import rx.Observable;
import rx.functions.Func1;

public class SortsMapper implements Func1<SortsDTO, List<String>>
{

    @Override
    public List<String> call(SortsDTO sortsDTO)
    {
        if (sortsDTO == null)
            return null;

        return sortsDTO.getSortData();
    }

}
