package kode.kinopoisk.pavlov.presenter.mappers;

import java.util.List;

import kode.kinopoisk.pavlov.model.dto.DetailFilmDTO;
import kode.kinopoisk.pavlov.presenter.BudgetData;
import kode.kinopoisk.pavlov.presenter.Cinema;
import kode.kinopoisk.pavlov.presenter.CinemaLocation;
import kode.kinopoisk.pavlov.presenter.Creators;
import kode.kinopoisk.pavlov.presenter.DetailFilm;
import kode.kinopoisk.pavlov.presenter.RatingData;
import kode.kinopoisk.pavlov.presenter.RentData;
import rx.Observable;
import rx.functions.Func1;

public class DetailFilmMapper implements Func1<DetailFilmDTO, DetailFilm>
{

    @Override
    public DetailFilm call(DetailFilmDTO detailFilmDTO)
    {
        if (detailFilmDTO == null)
            return null;

        RatingData ratingData = new RatingData(detailFilmDTO.getRatingDataDTO().getRating(), detailFilmDTO.getRatingDataDTO().getRatingVoteCount(), detailFilmDTO.getRatingDataDTO().getRatingAwait(), detailFilmDTO.getRatingDataDTO().getRatingAwaitCount(), detailFilmDTO.getRatingDataDTO().getRatingRFCritics(), detailFilmDTO.getRatingDataDTO().getRatingRFCriticsVoteCount());
        RentData rentData = new RentData(detailFilmDTO.getRentDataDTO().getPremiereRU(), detailFilmDTO.getRentDataDTO().getDistributors(), detailFilmDTO.getRentDataDTO().getPremiereWorld(), detailFilmDTO.getRentDataDTO().getPremiereWorldCountry());
        BudgetData budgetData = new BudgetData(detailFilmDTO.getBudgetDataDTO().getGrossRU(), detailFilmDTO.getBudgetDataDTO().getBudget());
        Creators creators = new Creators(detailFilmDTO.getCreatorsDTO().getDirectors(), detailFilmDTO.getCreatorsDTO().getActors(), detailFilmDTO.getCreatorsDTO().getProducers());
        List<Cinema> cinemaData = Observable.from(detailFilmDTO.getCinemaData())
                .map(cinemaDTO -> new Cinema(cinemaDTO.getCinemaName(), cinemaDTO.getSeanceData(), new CinemaLocation(cinemaDTO.getCinemaLocationDTO().getLon(), cinemaDTO.getCinemaLocationDTO().getLat())))
                .toList()
                .toBlocking()
                .first();

        return new DetailFilm(ratingData, detailFilmDTO.getFilmID(), detailFilmDTO.getPosterURL(), detailFilmDTO.getWebURL(), detailFilmDTO.getName(), detailFilmDTO.getYear(), detailFilmDTO.getFilmLength(), detailFilmDTO.getCountry(), detailFilmDTO.getGenre(), detailFilmDTO.getDescription(), detailFilmDTO.getRatingMPAA(), detailFilmDTO.getRatingAgeLimits(), rentData, budgetData, detailFilmDTO.getGallery(), creators, cinemaData);
    }

}
