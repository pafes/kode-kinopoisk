package kode.kinopoisk.pavlov.presenter;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class CinemaLocation {

    private String lon;
    private String lat;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CinemaLocation() {
    }

    /**
     * 
     * @param lon
     * @param lat
     */
    public CinemaLocation(String lon, String lat) {
        this.lon = lon;
        this.lat = lat;
    }

    /**
     * 
     * @return
     *     The lon
     */
    public String getLon() {
        return lon;
    }

    /**
     * 
     * @param lon
     *     The lon
     */
    public void setLon(String lon) {
        this.lon = lon;
    }

    /**
     * 
     * @return
     *     The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     * 
     * @param lat
     *     The lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(lon).append(lat).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;

        if (!(other instanceof CinemaLocation))
            return false;

        CinemaLocation rhs = ((CinemaLocation) other);

        return new EqualsBuilder().append(lon, rhs.lon).append(lat, rhs.lat).isEquals();
    }

}
