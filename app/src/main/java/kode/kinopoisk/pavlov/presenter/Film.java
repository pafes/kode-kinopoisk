
package kode.kinopoisk.pavlov.presenter;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class Film implements Serializable {

    private String filmID;
    private String name;
    private String webURL;
    private String rating;
    private String country;
    private String year;
    private String genre;
    private String filmLength;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Film() {
    }

    /**
     * 
     * @param genre
     * @param filmLength
     * @param name
     * @param webURL
     * @param year
     * @param rating
     * @param filmID
     * @param country
     */
    public Film(String filmID, String name, String webURL, String rating, String country, String year, String genre, String filmLength) {
        this.filmID = filmID;
        this.name = name;
        this.webURL = webURL;
        this.rating = rating;
        this.country = country;
        this.year = year;
        this.genre = genre;
        this.filmLength = filmLength;
    }

    /**
     * 
     * @return
     *     The filmID
     */
    public String getFilmID() {
        return filmID;
    }

    /**
     * 
     * @param filmID
     *     The filmID
     */
    public void setFilmID(String filmID) {
        this.filmID = filmID;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The webURL
     */
    public String getWebURL() {
        return webURL;
    }

    /**
     * 
     * @param webURL
     *     The webURL
     */
    public void setWebURL(String webURL) {
        this.webURL = webURL;
    }

    /**
     * 
     * @return
     *     The rating
     */
    public String getRating() {
        return rating;
    }

    /**
     * 
     * @param rating
     *     The rating
     */
    public void setRating(String rating) {
        this.rating = rating;
    }

    /**
     * 
     * @return
     *     The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The year
     */
    public String getYear() {
        return year;
    }

    /**
     * 
     * @param year
     *     The year
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * 
     * @return
     *     The genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * 
     * @param genre
     *     The genre
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * 
     * @return
     *     The filmLength
     */
    public String getFilmLength() {
        return filmLength;
    }

    /**
     * 
     * @param filmLength
     *     The filmLength
     */
    public void setFilmLength(String filmLength) {
        this.filmLength = filmLength;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(filmID).append(name).append(webURL).append(rating).append(country).append(year).append(genre).append(filmLength).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;

        if (!(other instanceof Film))
            return false;

        Film rhs = ((Film) other);

        return new EqualsBuilder().append(filmID, rhs.filmID).append(name, rhs.name).append(webURL, rhs.webURL).append(rating, rhs.rating).append(country, rhs.country).append(year, rhs.year).append(genre, rhs.genre).append(filmLength, rhs.filmLength).isEquals();
    }

}
