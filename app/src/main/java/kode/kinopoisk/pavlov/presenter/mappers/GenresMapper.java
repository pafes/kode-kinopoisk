package kode.kinopoisk.pavlov.presenter.mappers;

import java.util.List;

import kode.kinopoisk.pavlov.model.dto.GenresDTO;
import kode.kinopoisk.pavlov.presenter.Genre;
import rx.Observable;
import rx.functions.Func1;

public class GenresMapper implements Func1<GenresDTO, List<Genre>>
{

    @Override
    public List<Genre> call(GenresDTO genresDTO)
    {
        if (genresDTO == null)
            return null;

        return Observable.from(genresDTO.getGenreData())
            .map(genreDTO -> new Genre(genreDTO.getGenreID(), genreDTO.getGenreName()))
            .toList()
            .toBlocking()
            .first();
    }

}
