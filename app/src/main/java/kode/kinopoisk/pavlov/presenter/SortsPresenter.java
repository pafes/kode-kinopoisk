package kode.kinopoisk.pavlov.presenter;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kode.kinopoisk.pavlov.presenter.mappers.SortsMapper;
import kode.kinopoisk.pavlov.view.View;

import rx.Observer;
import rx.Subscription;

public class SortsPresenter extends BasePresenter
{

    private View view;
    private List<String> sortList;

    public SortsPresenter(View view)
    {
        this.view = view;
        this.sortList = new ArrayList<>();

        SortsMapper mapper = new SortsMapper();

        initSortList(view, mapper);
    }

    private void initSortList(final View view, SortsMapper mapper)
    {
        Subscription subscription = model.getSorts()
            .map(mapper)
            .subscribe(new Observer<List<String>>()
            {

                @Override
                public void onCompleted()
                {}

                @Override
                public void onError(Throwable e)
                {
                    Log.d(SortsPresenter.class.getName(), e.toString());
                    showError(e);
                }

                @Override
                public void onNext(List<String> list)
                {
                    if (list == null || list.isEmpty())
                        view.showError("Не удается загрузить список сортировок");
                    else
                        sortList = list;
                }

            });

        addSubscription(subscription);
    }

    public List<String> getSortList()
    {
        return sortList;
    }

    @Override
    protected View getView()
    {
        return view;
    }

}
