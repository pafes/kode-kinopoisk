
package kode.kinopoisk.pavlov.presenter;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class RentData implements Serializable {

    private String premiereRU;
    private String distributors;
    private String premiereWorld;
    private String premiereWorldCountry;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RentData() {
    }

    /**
     * 
     * @param premiereWorld
     * @param premiereRU
     * @param premiereWorldCountry
     * @param distributors
     */
    public RentData(String premiereRU, String distributors, String premiereWorld, String premiereWorldCountry) {
        this.premiereRU = premiereRU;
        this.distributors = distributors;
        this.premiereWorld = premiereWorld;
        this.premiereWorldCountry = premiereWorldCountry;
    }

    /**
     * 
     * @return
     *     The premiereRU
     */
    public String getPremiereRU() {
        return premiereRU;
    }

    /**
     * 
     * @param premiereRU
     *     The premiereRU
     */
    public void setPremiereRU(String premiereRU) {
        this.premiereRU = premiereRU;
    }

    /**
     * 
     * @return
     *     The distributors
     */
    public String getDistributors() {
        return distributors;
    }

    /**
     * 
     * @param distributors
     *     The Distributors
     */
    public void setDistributors(String distributors) {
        this.distributors = distributors;
    }

    /**
     * 
     * @return
     *     The premiereWorld
     */
    public String getPremiereWorld() {
        return premiereWorld;
    }

    /**
     * 
     * @param premiereWorld
     *     The premiereWorld
     */
    public void setPremiereWorld(String premiereWorld) {
        this.premiereWorld = premiereWorld;
    }

    /**
     * 
     * @return
     *     The premiereWorldCountry
     */
    public String getPremiereWorldCountry() {
        return premiereWorldCountry;
    }

    /**
     * 
     * @param premiereWorldCountry
     *     The premiereWorldCountry
     */
    public void setPremiereWorldCountry(String premiereWorldCountry) {
        this.premiereWorldCountry = premiereWorldCountry;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(premiereRU).append(distributors).append(premiereWorld).append(premiereWorldCountry).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;

        if (!(other instanceof RentData))
            return false;

        RentData rhs = ((RentData) other);

        return new EqualsBuilder().append(premiereRU, rhs.premiereRU).append(distributors, rhs.distributors).append(premiereWorld, rhs.premiereWorld).append(premiereWorldCountry, rhs.premiereWorldCountry).isEquals();
    }

}
