package kode.kinopoisk.pavlov.presenter.mappers;

import java.util.List;

import kode.kinopoisk.pavlov.model.dto.CityListDTO;
import kode.kinopoisk.pavlov.presenter.City;

import rx.Observable;
import rx.functions.Func1;

public class CityListMapper implements Func1<CityListDTO, List<City>>
{

    @Override
    public List<City> call(CityListDTO cityListDTO)
    {
        if (cityListDTO == null)
            return null;

        return Observable.from(cityListDTO.getCityData())
            .map(cityDTO -> new City(cityDTO.getCityID(), cityDTO.getCityName()))
            .toList()
            .toBlocking()
            .first();
    }

}
