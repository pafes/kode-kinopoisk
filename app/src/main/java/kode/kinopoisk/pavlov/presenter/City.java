package kode.kinopoisk.pavlov.presenter;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class City implements Serializable {

    private String cityID;
    private String cityName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public City() {
    }

    /**
     * 
     * @param cityName
     * @param cityID
     */
    public City(String cityID, String cityName) {
        this.cityID = cityID;
        this.cityName = cityName;
    }

    /**
     * 
     * @return
     *     The cityID
     */
    public String getCityID() {
        return cityID;
    }

    /**
     * 
     * @param cityID
     *     The cityID
     */
    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    /**
     * 
     * @return
     *     The cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * 
     * @param cityName
     *     The cityName
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(cityID).append(cityName).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;

        if (!(other instanceof City))
            return false;

        City rhs = ((City) other);

        return new EqualsBuilder().append(cityID, rhs.cityID).append(cityName, rhs.cityName).isEquals();
    }

}
