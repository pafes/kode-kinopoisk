
package kode.kinopoisk.pavlov.presenter;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Creators implements Serializable {

    private List<String> directors = new ArrayList<>();
    private List<String> actors = new ArrayList<>();
    private List<String> producers = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Creators() {
    }

    /**
     * 
     * @param actors
     * @param directors
     * @param producers
     */
    public Creators(List<String> directors, List<String> actors, List<String> producers) {
        this.directors = directors;
        this.actors = actors;
        this.producers = producers;
    }

    /**
     * 
     * @return
     *     The directors
     */
    public List<String> getDirectors() {
        return directors;
    }

    /**
     * 
     * @param directors
     *     The directors
     */
    public void setDirectors(List<String> directors) {
        this.directors = directors;
    }

    /**
     * 
     * @return
     *     The actors
     */
    public List<String> getActors() {
        return actors;
    }

    /**
     * 
     * @param actors
     *     The actors
     */
    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    /**
     * 
     * @return
     *     The producers
     */
    public List<String> getProducers() {
        return producers;
    }

    /**
     * 
     * @param producers
     *     The producers
     */
    public void setProducers(List<String> producers) {
        this.producers = producers;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(directors).append(actors).append(producers).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;

        if (!(other instanceof Creators))
            return false;

        Creators rhs = ((Creators) other);

        return new EqualsBuilder().append(directors, rhs.directors).append(actors, rhs.actors).append(producers, rhs.producers).isEquals();
    }

    @Override
    public String toString()
    {
        String directors = TextUtils.join(", ", getDirectors());
        String actors = TextUtils.join(", ", getActors());
        String producers = TextUtils.join(", ", getProducers());
        String directorsLine = "Режиссеры: " + directors;
        String actorsLine = "Актеры: " + actors;
        String producersLine = "Продюсеры: " + producers;

        return directorsLine + "\n" + actorsLine + "\n" + producersLine;
    }

}
