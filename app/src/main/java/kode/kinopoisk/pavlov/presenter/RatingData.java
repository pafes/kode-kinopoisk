
package kode.kinopoisk.pavlov.presenter;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class RatingData implements Serializable {

    private String rating;
    private String ratingVoteCount;
    private String ratingAwait;
    private String ratingAwaitCount;
    private String ratingRFCritics;
    private String ratingRFCriticsVoteCount;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RatingData() {
    }

    /**
     * 
     * @param ratingRFCriticsVoteCount
     * @param ratingVoteCount
     * @param ratingAwaitCount
     * @param ratingRFCritics
     * @param rating
     * @param ratingAwait
     */
    public RatingData(String rating, String ratingVoteCount, String ratingAwait, String ratingAwaitCount, String ratingRFCritics, String ratingRFCriticsVoteCount) {
        this.rating = rating;
        this.ratingVoteCount = ratingVoteCount;
        this.ratingAwait = ratingAwait;
        this.ratingAwaitCount = ratingAwaitCount;
        this.ratingRFCritics = ratingRFCritics;
        this.ratingRFCriticsVoteCount = ratingRFCriticsVoteCount;
    }

    /**
     * 
     * @return
     *     The rating
     */
    public String getRating() {
        return rating;
    }

    /**
     * 
     * @param rating
     *     The rating
     */
    public void setRating(String rating) {
        this.rating = rating;
    }

    /**
     * 
     * @return
     *     The ratingVoteCount
     */
    public String getRatingVoteCount() {
        return ratingVoteCount;
    }

    /**
     * 
     * @param ratingVoteCount
     *     The ratingVoteCount
     */
    public void setRatingVoteCount(String ratingVoteCount) {
        this.ratingVoteCount = ratingVoteCount;
    }

    /**
     * 
     * @return
     *     The ratingAwait
     */
    public String getRatingAwait() {
        return ratingAwait;
    }

    /**
     * 
     * @param ratingAwait
     *     The ratingAwait
     */
    public void setRatingAwait(String ratingAwait) {
        this.ratingAwait = ratingAwait;
    }

    /**
     * 
     * @return
     *     The ratingAwaitCount
     */
    public String getRatingAwaitCount() {
        return ratingAwaitCount;
    }

    /**
     * 
     * @param ratingAwaitCount
     *     The ratingAwaitCount
     */
    public void setRatingAwaitCount(String ratingAwaitCount) {
        this.ratingAwaitCount = ratingAwaitCount;
    }

    /**
     * 
     * @return
     *     The ratingRFCritics
     */
    public String getRatingRFCritics() {
        return ratingRFCritics;
    }

    /**
     * 
     * @param ratingRFCritics
     *     The ratingRFCritics
     */
    public void setRatingRFCritics(String ratingRFCritics) {
        this.ratingRFCritics = ratingRFCritics;
    }

    /**
     * 
     * @return
     *     The ratingRFCriticsVoteCount
     */
    public String getRatingRFCriticsVoteCount() {
        return ratingRFCriticsVoteCount;
    }

    /**
     * 
     * @param ratingRFCriticsVoteCount
     *     The ratingRFCriticsVoteCount
     */
    public void setRatingRFCriticsVoteCount(String ratingRFCriticsVoteCount) {
        this.ratingRFCriticsVoteCount = ratingRFCriticsVoteCount;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(rating).append(ratingVoteCount).append(ratingAwait).append(ratingAwaitCount).append(ratingRFCritics).append(ratingRFCriticsVoteCount).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;

        if (!(other instanceof RatingData))
            return false;

        RatingData rhs = ((RatingData) other);

        return new EqualsBuilder().append(rating, rhs.rating).append(ratingVoteCount, rhs.ratingVoteCount).append(ratingAwait, rhs.ratingAwait).append(ratingAwaitCount, rhs.ratingAwaitCount).append(ratingRFCritics, rhs.ratingRFCritics).append(ratingRFCriticsVoteCount, rhs.ratingRFCriticsVoteCount).isEquals();
    }

}
