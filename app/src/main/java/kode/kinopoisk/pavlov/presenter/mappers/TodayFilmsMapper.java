package kode.kinopoisk.pavlov.presenter.mappers;

import java.util.List;

import kode.kinopoisk.pavlov.model.dto.TodayFilmsDTO;
import kode.kinopoisk.pavlov.presenter.Film;
import rx.Observable;
import rx.functions.Func1;

public class TodayFilmsMapper implements Func1<TodayFilmsDTO, List<Film>>
{

    @Override
    public List<Film> call(TodayFilmsDTO todayFilmsDTO)
    {
        if (todayFilmsDTO == null)
            return null;

        return Observable.from(todayFilmsDTO.getFilmData())
            .map(filmDTO -> new Film(filmDTO.getFilmID(), filmDTO.getName(), filmDTO.getWebURL(), filmDTO.getRating(), filmDTO.getCountry(), filmDTO.getYear(), filmDTO.getGenre(), filmDTO.getFilmLength()))
            .toList()
            .toBlocking()
            .first();
    }

}
