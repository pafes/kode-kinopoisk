package kode.kinopoisk.pavlov.presenter;

import android.util.Log;

import kode.kinopoisk.pavlov.presenter.mappers.DetailFilmMapper;
import kode.kinopoisk.pavlov.view.DetailFilmView;
import kode.kinopoisk.pavlov.view.View;

import rx.Observer;
import rx.Subscription;

public class DetailFilmPresenter extends BasePresenter
{

    private DetailFilmView view;
    private DetailFilmMapper mapper;

    public DetailFilmPresenter(DetailFilmView view)
    {
        this.view = view;

        mapper = new DetailFilmMapper();
    }

    public void loadFilm(int filmID)
    {
        Subscription subscription = model.getDetailFilm(filmID)
            .map(mapper)
            .subscribe(new Observer<DetailFilm>()
            {

                @Override
                public void onCompleted()
                {}

                @Override
                public void onError(Throwable e)
                {
                    Log.d(DetailFilmPresenter.class.getName(), e.toString());
                    showError(e);
                }

                @Override
                public void onNext(DetailFilm detailFilm)
                {
                    view.showDetailFilm(detailFilm);
                }

            });

        addSubscription(subscription);
    }

    @Override
    protected View getView()
    {
        return view;
    }

}
