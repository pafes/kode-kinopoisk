package kode.kinopoisk.pavlov.presenter;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class BudgetData implements Serializable {

    private String grossRU;
    private String budget;

    /**
     * No args constructor for use in serialization
     * 
     */
    public BudgetData() {
    }

    /**
     * 
     * @param grossRU
     * @param budget
     */
    public BudgetData(String grossRU, String budget) {
        this.grossRU = grossRU;
        this.budget = budget;
    }

    /**
     * 
     * @return
     *     The grossRU
     */
    public String getGrossRU() {
        return grossRU;
    }

    /**
     * 
     * @param grossRU
     *     The grossRU
     */
    public void setGrossRU(String grossRU) {
        this.grossRU = grossRU;
    }

    /**
     * 
     * @return
     *     The budget
     */
    public String getBudget() {
        return budget;
    }

    /**
     * 
     * @param budget
     *     The budget
     */
    public void setBudget(String budget) {
        this.budget = budget;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(grossRU).append(budget).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;

        if (!(other instanceof BudgetData))
            return false;

        BudgetData rhs = ((BudgetData) other);

        return new EqualsBuilder().append(grossRU, rhs.grossRU).append(budget, rhs.budget).isEquals();
    }

}
