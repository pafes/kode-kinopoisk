package kode.kinopoisk.pavlov.presenter;

import kode.kinopoisk.pavlov.model.Model;
import kode.kinopoisk.pavlov.model.ModelImpl;
import kode.kinopoisk.pavlov.model.api.ApiInterface;
import kode.kinopoisk.pavlov.model.api.ApiModule;
import kode.kinopoisk.pavlov.other.Const;
import kode.kinopoisk.pavlov.view.View;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter implements Presenter
{

    protected Model model;
    protected CompositeSubscription compositeSubscription;

    public BasePresenter()
    {
        ApiInterface api = ApiModule.getApiInterface(Const.BASE_URL);

        model = new ModelImpl(api);
        compositeSubscription = new CompositeSubscription();
    }

    protected void addSubscription(Subscription subscription)
    {
        compositeSubscription.add(subscription);
    }

    @Override
    public void onStop()
    {
        compositeSubscription.clear();
    }

    protected abstract View getView();

    protected void showError(Throwable e)
    {
        getView().showError(e.getMessage());
    }

}
