package kode.kinopoisk.pavlov.presenter;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kode.kinopoisk.pavlov.presenter.mappers.CityListMapper;
import kode.kinopoisk.pavlov.view.CityListView;
import kode.kinopoisk.pavlov.view.View;
import rx.Observable;
import rx.Observer;
import rx.Subscription;

public class CitiesPresenter extends BasePresenter implements TextView.OnEditorActionListener
{

    private CityListView view;
    private CityListMapper mapper;
    private List<City> cityList;

    public TextWatcher textChangedListener = new TextWatcher()
    {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
        {}

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
        {}

        @Override
        public void afterTextChanged(Editable s)
        {
            onSearchButtonClick();
        }

    };

    public CitiesPresenter(CityListView view)
    {
        this.view = view;

        mapper = new CityListMapper();
    }

    public void initCityList()
    {
        Subscription subscription = model.getCityList()
            .map(mapper)
            .subscribe(new Observer<List<City>>()
            {

                @Override
                public void onCompleted()
                {}

                @Override
                public void onError(Throwable e)
                {
                    Log.d(CitiesPresenter.class.getName(), e.toString());
                    showError(e);
                }

                @Override
                public void onNext(List<City> list)
                {
                    if (list == null || list.isEmpty())
                        view.showEmptyList();
                    else
                    {
                        cityList = list;

                        view.showList(cityList);
                    }
                }

            });

        addSubscription(subscription);
    }

    private void onSearchButtonClick()
    {
        String cityName = view.getCityName();

        if (TextUtils.isEmpty(cityName))
            view.showList(cityList);
        else
            initFilteredCityList(cityName);
    }

    private void initFilteredCityList(String cityName)
    {
        ArrayList<City> filteredCityList = new ArrayList<>();

        Subscription subscription = Observable.from(cityList)
            .filter(city -> city.getCityName().toLowerCase().contains(cityName.toLowerCase()))
            .doOnNext(filteredCityList::add)
            .doOnCompleted(() -> {
                if (filteredCityList.isEmpty())
                    view.showError("Город не найден");
                else
                    view.showList(filteredCityList);
            })
            .subscribe();

        addSubscription(subscription);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
    {
        if (actionId == EditorInfo.IME_ACTION_SEARCH)
        {
            onSearchButtonClick();

            return true;
        }

        return false;
    }

    @Override
    protected View getView()
    {
        return view;
    }

}
