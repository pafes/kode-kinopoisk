package kode.kinopoisk.pavlov.presenter;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kode.kinopoisk.pavlov.presenter.mappers.GenresMapper;
import kode.kinopoisk.pavlov.view.View;

import rx.Observer;
import rx.Subscription;

public class GenresPresenter extends BasePresenter
{

    private View view;
    private List<Genre> genreList;

    public GenresPresenter(View view)
    {
        this.view = view;
        this.genreList = new ArrayList<>();

        GenresMapper mapper = new GenresMapper();

        initGenreList(view, mapper);
    }

    private void initGenreList(View view, GenresMapper mapper)
    {
        Subscription subscription = model.getGenres()
            .map(mapper)
            .subscribe(new Observer<List<Genre>>()
            {

                @Override
                public void onCompleted()
                {}

                @Override
                public void onError(Throwable e)
                {
                    Log.d(GenresPresenter.class.getName(), e.toString());
                    showError(e);
                }

                @Override
                public void onNext(List<Genre> list)
                {
                    if (list == null || list.isEmpty())
                        view.showError("Не удается загрузить список жанров");
                    else
                        genreList = list;
                }

            });

        addSubscription(subscription);
    }

    public List<Genre> getGenreList()
    {
        return genreList;
    }

    @Override
    protected View getView()
    {
        return view;
    }

}
