
package kode.kinopoisk.pavlov.presenter;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class Genre implements Serializable {

    private String genreID;
    private String genreName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Genre() {
    }

    /**
     * 
     * @param genreName
     * @param genreID
     */
    public Genre(String genreID, String genreName) {
        this.genreID = genreID;
        this.genreName = genreName;
    }

    /**
     * 
     * @return
     *     The genreID
     */
    public String getGenreID() {
        return genreID;
    }

    /**
     * 
     * @param genreID
     *     The genreID
     */
    public void setGenreID(String genreID) {
        this.genreID = genreID;
    }

    /**
     * 
     * @return
     *     The genreName
     */
    public String getGenreName() {
        return genreName;
    }

    /**
     * 
     * @param genreName
     *     The genreName
     */
    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(genreID).append(genreName).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;

        if (!(other instanceof Genre))
            return false;

        Genre rhs = ((Genre) other);

        return new EqualsBuilder().append(genreID, rhs.genreID).append(genreName, rhs.genreName).isEquals();
    }

}
