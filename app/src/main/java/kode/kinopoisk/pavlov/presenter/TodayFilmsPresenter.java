package kode.kinopoisk.pavlov.presenter;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kode.kinopoisk.pavlov.presenter.mappers.TodayFilmsMapper;
import kode.kinopoisk.pavlov.view.ListView;
import kode.kinopoisk.pavlov.view.View;

import rx.Observer;
import rx.Subscription;

public class TodayFilmsPresenter extends BasePresenter
{

    private ListView<Film> view;
    private TodayFilmsMapper mapper;
    private int cityID;
    private List<Integer> genreIDs;
    private String sortBy;

    public TodayFilmsPresenter(ListView<Film> view)
    {
        this.view = view;

        mapper = new TodayFilmsMapper();
        cityID = -1;
        genreIDs = new ArrayList<>();
        sortBy = "";
    }

    public void setCityID(int cityID)
    {
        this.cityID = cityID;
    }

    public void setGenreIDs(List<Integer> genreIDs)
    {
        this.genreIDs = genreIDs;
    }

    public void setSortBy(String sortBy)
    {
        this.sortBy = sortBy;
    }

    public void loadTodayFilms()
    {
        Subscription subscription = model.getTodayFilms(cityID, genreIDs, sortBy)
            .map(mapper)
            .subscribe(new Observer<List<Film>>()
            {

                @Override
                public void onCompleted()
                {}

                @Override
                public void onError(Throwable e)
                {
                    Log.d(TodayFilmsPresenter.class.getName(), e.toString());
                    showError(e);
                }

                @Override
                public void onNext(List<Film> list)
                {
                    if (list != null && !list.isEmpty())
                        view.showList(list);
                    else
                        view.showEmptyList();
                }

            });

        addSubscription(subscription);
    }

    @Override
    protected View getView()
    {
        return view;
    }

}
